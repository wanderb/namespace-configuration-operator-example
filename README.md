# namespace-config-operator examples

This repo contains some examples for utilizing the
[namespace-config-operator](https://github.com/redhat-cop/namespace-configuration-operator)
on OpenShift. All examples are based on the examples in that repo.

These examples will do the following:

- Every namespace with a label `size` set to either `xs`, `s`, `m`, or `l` will
  get the appropriate ResourceQuota applied
- Every namespace with the label `multitenant=true` will get a set of default
  NetworkPolicies applied.
- Every user with label `sandbox-true` will get a project called
  `<user>-sandbox` with a label `size=xs`, thus getting a ResourceQuota
  applied, and a label `multitenant=true`, resulting in a set of default
  NetworkPolcies.
- Every group with the label `type=devteam` will get a set of four, multitenant, namespaces:
  - `<name>-cicd` for their pipelines and builds
  - `<name>-dev` for their dev deployments
  - `<name>-test` for testing
  - `<name>-prod` for production
  These namespaces are labelled according to their use (build/run), and
  RoleBindings are set up so that the Tekton serviceaccount has full access to
  all projects, but developers only have `view` permissions on production,
  `edit` on test, and `admin` on cicd and dev.

  A ClusterResourceQuota is also setup, spanning all namespaces for the team.


## Setup
1. Create a project for the operator

   ```bash
   oc new-project namespace-configuration-operator
   ```
   
2. Add namespace-config-operator operator from operatorhub
3. Apply the example policies:

   ```bash
   oc apply -f examples
   ```
   

## Testing

### UserConfig and NamespaceConfig
1. Add a `sandbox=true` label to a user

   ```bash
   oc label user developer sandbox=true
   ```
2. Check if a sandbox namespace has been created for the user, with a `xs`
   project quota, and a set of default network policies.
   
   ```bash
   oc get resourcequota,netpol -n developer-sandbox
   ```

### GroupConfig
1. Create a new group, and label it with `type=devteam`

   ```bash
   oc adm groups new flopsels developer
   oc label group flopsels type=devteam

   ```
2. Check if the correct resources are created:

   ```bash
   for NS in flopsels-{cicd,dev,test,prod}; do oc -n ${NS} get netpol,rolebinding; done
   oc get clusterresourcequota flopsels-quota
   ```


